const express = require('express');

const Photo = require("../models/Photo");
const auth = require("../middleware/auth");

const upload = require('../multer').photos;

const router = express.Router();

router.get('/all', async (req, res) => {
   try {
       const criteria = {};

       if(req.query.user) {
           criteria.user = req.query.user;
       }

       const photos = await Photo.find(criteria).populate('user');

       res.send(photos);
   } catch (e) {
       res.sendStatus(500);
   }
});

router.get('/users/:id', auth, async (req, res) => {
   try {
       const photo = await Photo.findOne({_id: req.params._id}).populate('user');

       if (photo) {
           res.send(photo)
       } else {
           res.sendStatus(400);
       }

   } catch (e) {
       res.sendStatus(500);
   }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
   try {
       const photoReqData = req.body;
       photoReqData.user = req.user._id;

       if (req.file) {
           photoReqData.image = req.file.filename;
       }

       const photo = new Photo(photoReqData);
       await photo.save();

       res.send(photo);

   } catch (e) {
       res.status(400).send(e);
   }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const reqPhoto = req.body;
        reqPhoto.user = req.user._id;

        if (reqPhoto.user) {
            const photo = await Photo.findOne({_id: req.params.id}).remove();
            res.send(photo);
        } else {
            res.sendStatus(403);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;