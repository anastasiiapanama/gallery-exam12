import axiosApi from "../../axios-api";
import {NotificationManager} from "react-notifications";
import {historyPush} from "./historyaActions";

export const FETCH_PHOTOS_REQUEST = 'FETCH_PHOTOS_REQUEST';
export const FETCH_PHOTOS_SUCCESS = 'FETCH_PHOTOS_SUCCESS';
export const FETCH_PHOTOS_FAILURE = 'FETCH_PHOTOS_FAILURE';

export const DELETE_PHOTO_SUCCESS = 'DELETE_PHOTO_SUCCESS';

export const CREATE_PHOTO_REQUEST = 'CREATE_PHOTO_REQUEST';
export const CREATE_PHOTO_SUCCESS = 'CREATE_PHOTO_SUCCESS';
export const CREATE_PHOTO_FAILURE = 'CREATE_PHOTO_FAILURE';

export const fetchPhotosRequest = () => ({type: FETCH_PHOTOS_REQUEST});
export const fetchPhotosSuccess = photos => ({type: FETCH_PHOTOS_SUCCESS, photos});
export const fetchPhotosFailure = () => ({type: FETCH_PHOTOS_FAILURE});

export const deletePhotoSuccess = id => ({type: DELETE_PHOTO_SUCCESS, id});

export const createPhotoRequest = () => ({type: CREATE_PHOTO_REQUEST});
export const createPhotoSuccess = () => ({type: CREATE_PHOTO_SUCCESS});
export const createPhotoFailure = error => ({type: CREATE_PHOTO_FAILURE, error});

export const fetchPhotos = userId => {
    return async dispatch => {
        try {
            let url = '/photos/all';

            if (userId) {
                url += '?user=' + userId;
            }

            dispatch(fetchPhotosRequest());
            const response = await axiosApi.get(url);
            dispatch(fetchPhotosSuccess(response.data));

        } catch (e) {
            dispatch(fetchPhotosFailure());
            NotificationManager.error('Could not get photos');
        }
    }
};

export const deleteUserPhoto = (photoId) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            await axiosApi.delete('/photos/' + photoId, {headers});
            dispatch(deletePhotoSuccess(photoId));

            NotificationManager.success('Photo deleted successfully');
        } catch (e) {
            NotificationManager.error('Could not delete photo');
        }
    }
};

export const createPhoto = photoData => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {'Authorization': token};

            dispatch(createPhotoRequest());
            await axiosApi.post('/photos', photoData, {headers});
            dispatch(createPhotoSuccess());
            dispatch(historyPush('/'));

            NotificationManager.success('Photo created successfully');
        } catch (e) {
            dispatch(createPhotoFailure(e.response.data));
            NotificationManager.error('Could not create new photo');
        }
    }
};
