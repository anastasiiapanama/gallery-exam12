import React, {useEffect} from 'react';
import {Link, useParams} from "react-router-dom";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch, useSelector} from "react-redux";
import {deleteUserPhoto, fetchPhotos} from "../../store/actions/photosActions";
import Grid from "@material-ui/core/Grid";
import {CircularProgress, Typography} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import UserPhotoItem from "./UserPhotoItem/UserPhotoItem";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const UserGallery = () => {
    const classes = useStyles();
    const params = useParams();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const userPhotos = useSelector(state => state.photos.photos);
    const loading = useSelector(state => state.photos.photosLoading);

    useEffect(() => {
        dispatch(fetchPhotos(params.id));
    }, [dispatch, params.id]);

    const onDeleteHandler = (photoId) => {
        dispatch(deleteUserPhoto(photoId));
    }

    return (
        <Grid container direction="column" spacing={2}>
            {user._id === params.id && (
                <Grid item container justify="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h4">{user.displayName}'s photos</Typography>
                    </Grid>

                    <Grid item>
                        <Button color="primary" component={Link} to="/photos/new">Add new photo</Button>
                    </Grid>
                </Grid>
            )}

            <Grid item container spacing={1}>
                {loading ? (
                    <Grid container justify="center" alignItems="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) : userPhotos.map(photo => (
                    <UserPhotoItem
                        key={photo._id}
                        id={photo.user._id}
                        title={photo.title}
                        image={photo.image}
                        deleteButton={() => onDeleteHandler(photo._id)}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default UserGallery;