const mongoose = require('mongoose');

const PhotoSchema = new mongoose.Schema({
    user: {
        required: true,
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    title: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    }
});

const Photo = mongoose.model('Photo', PhotoSchema);
module.exports = Photo;