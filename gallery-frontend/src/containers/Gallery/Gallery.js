import React, {useEffect} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import {CircularProgress, Typography} from "@material-ui/core";
import {fetchPhotos} from "../../store/actions/photosActions";
import PhotoItem from "./PhotoItem/PhotoItem";
import {useParams} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const Gallery = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const params = useParams();
    const photos = useSelector(state => state.photos.photos);
    const user = useSelector(state => state.users.user);
    const loading = useSelector(state => state.photos.photosLoading);

    useEffect(() => {
        dispatch(fetchPhotos(params.id));
    }, [dispatch, params.id]);

    return (
        <Grid item container justify="space-between" alignItems="center" spacing={3}>
            <Grid item>
                <Typography variant="h4">Photos</Typography>
            </Grid>

            {user ? (
                <Grid item container spacing={4}>
                    {loading ? (
                        <Grid container justify="center" alignItems="center" className={classes.progress}>
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : photos && photos.map(photo => (
                        <PhotoItem
                            key={photo._id}
                            id={photo.user._id}
                            title={photo.title}
                            image={photo.image}
                            author={photo.user.displayName}
                        />
                    ))}
                </Grid>
            ) : (
                <Grid container justify="center" alignItems="center" className={classes.progress}>
                    <Grid item>
                        <Typography>Sign in to watch photo Gallery</Typography>
                    </Grid>
                </Grid>
            )}
        </Grid>
    );
};

export default Gallery;