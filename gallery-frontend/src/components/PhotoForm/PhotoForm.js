import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormElement from "../UI/Form/FormElement";
import FileInput from "../UI/Form/FileInput";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const PhotoForm = ({onSubmit, loading, error}) => {
    const [photo, setPhoto] = useState({
        title: '',
        image: '',
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(photo).forEach(key => {
            formData.append(key, photo[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;

        setPhoto(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setPhoto(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <form onSubmit={submitFormHandler} noValidate>
            <Grid container direction="column" spacing={2}>

                <FormElement
                    required
                    label="Title"
                    name="title"
                    value={photo.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}
                />

                <Grid item xs>
                    <FileInput
                        name="image"
                        label="Image"
                        onChange={fileChangeHandler}
                        error={getFieldError('image')}
                    />
                </Grid>

                <Grid item xs>
                    <ButtonWithProgress
                        type="submit" color="primary" variant="contained"
                        loading={loading} disabled={loading}
                    >
                        Add to gallery
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </form>
    );
};

export default PhotoForm;