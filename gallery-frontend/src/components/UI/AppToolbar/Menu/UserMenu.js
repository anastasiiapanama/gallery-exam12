import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {Avatar, IconButton, makeStyles, Menu, MenuItem} from "@material-ui/core";
import {apiURL} from "../../../../config";
import {logoutUser} from "../../../../store/actions/usersActions";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
    avatar: {
        width: theme.spacing(4),
        height: theme.spacing(4)
    }
}));

const UserMenu = ({user}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <Button color="inherit">Hello, {user.displayName}!</Button>

            <IconButton
                onClick={handleClick}
                color="inherit"
            >
                {user.avatar ?
                    <Avatar
                        src={apiURL + '/' + user.avatar}
                        className={classes.avatar}
                    />
                    :
                    <Avatar className={classes.avatar}/>
                }
            </IconButton>

            <Menu
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem disabled>{user.displayName}</MenuItem>
                <MenuItem component={Link} to={'/photos/users/' + user._id}>My Library</MenuItem>
                {user && (
                    <MenuItem component={Link} to={'/photos/new'}>Add new photo</MenuItem>
                )}
                <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;