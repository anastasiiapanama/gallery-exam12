import React from 'react';
import {Switch, Route} from "react-router-dom";

import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Gallery from "./containers/Gallery/Gallery";
import UserGallery from "./containers/UserGallery/UserGallery";
import NewPhoto from "./containers/NewPhoto/NewPhoto";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";

const App = () => (
    <>
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={Gallery}/>
            <Route path="/photos/users/:id" exact component={UserGallery}/>
            <Route path="/photos/new" exact component={NewPhoto}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
          </Switch>
        </Container>
      </main>
    </>
);

export default App;
