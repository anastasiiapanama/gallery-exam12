const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');

const User = require("./models/User");
const Photo = require("./models/Photo");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [John, Sara] = await User.create({
        email: 'john@1',
        password: '123',
        token: nanoid(),
        displayName: 'John Doe'
    }, {
        email: 'sara@1',
        password: '123',
        token: nanoid(),
        displayName: 'Sara Black'
    });

    await Photo.create({
        user: John,
        title: 'Beautiful Nature',
        image: 'photos/img1.jpeg'
    }, {
        user: John,
        title: 'Beautiful Nature',
        image: 'photos/img2.jpeg'
    }, {
        user: John,
        title: 'Beautiful Nature',
        image: 'photos/img3.jpeg'
    }, {
        user: John,
        title: 'Beautiful Nature',
        image: 'photos/img4.png'
    }, {
        user: Sara,
        title: 'Beautiful Japan',
        image: 'photos/img11.jpeg'
    }, {
        user: Sara,
        title: 'Beautiful Japan',
        image: 'photos/img22.jpeg'
    }, {
        user: Sara,
        title: 'Beautiful Japan',
        image: 'photos/img33.jpeg'
    });

    await mongoose.connection.close();
};

run().catch(console.error);