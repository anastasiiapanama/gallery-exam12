import {
    CREATE_PHOTO_FAILURE,
    CREATE_PHOTO_REQUEST, CREATE_PHOTO_SUCCESS,
    DELETE_PHOTO_SUCCESS,
    FETCH_PHOTOS_FAILURE,
    FETCH_PHOTOS_REQUEST,
    FETCH_PHOTOS_SUCCESS
} from "../actions/photosActions";

const initialState = {
    photos: [],
    photosLoading: false,
    createPhotoLoading: false,
    createPhotoError: null
};

const photosReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PHOTOS_REQUEST:
            return {...state, photosLoading: true};
        case FETCH_PHOTOS_SUCCESS:
            return {...state, photosLoading: false, photos: action.photos};
        case FETCH_PHOTOS_FAILURE:
            return {...state, photosLoading: false};
        case DELETE_PHOTO_SUCCESS:
            return {...state, photos: state.photos.filter(c => c.id !== action.id)};
        case CREATE_PHOTO_REQUEST:
            return {...state, createPhotoLoading: true};
        case CREATE_PHOTO_SUCCESS:
            return {...state, createPhotoLoading: false};
        case CREATE_PHOTO_FAILURE:
            return {...state, createPhotoLoading: false, createPhotoError: action.error};
        default:
            return state;
    }
};

export default photosReducer;