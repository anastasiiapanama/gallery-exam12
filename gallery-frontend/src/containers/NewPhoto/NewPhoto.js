import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {createPhoto} from "../../store/actions/photosActions";

import Grid from "@material-ui/core/Grid";
import {Typography} from "@material-ui/core";

const NewPhoto = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.photos.createPhotoError);
    const loading = useSelector(state => state.photos.createPhotoLoading);

    const onPhotoFormSubmit = async photoData => {
        dispatch(createPhoto(photoData));
    }

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h4">Add new photo</Typography>
            </Grid>

            <Grid item xs>
                <PhotoForm
                    onSubmit={onPhotoFormSubmit}
                    error={error}
                    loading={loading}
                />
            </Grid>
        </Grid>
    );
};

export default NewPhoto;