import React, {useState} from 'react';
import {apiURL} from "../../../config";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import {
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    CardMedia,
    IconButton,
    Modal,
    Typography
} from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import CancelPresentationIcon from "@material-ui/icons/CancelPresentation";


const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 250,
    },
    link: {
        textDecoration: 'none',
        paddingLeft: 8,
        color: "inherit"
    },
    modal: {
        marginTop: 200
    },
    modalMedia: {
        height: 300
    },
    closeButton: {
        justifyContent: 'center'
    }
});

const UserPhotoItem = ({title, image, deleteButton}) => {
    const params = useParams();
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const user = useSelector(state => state.users.user);

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.root}>
                <CardActionArea onClick={() => setOpen(true)}>
                    <CardMedia
                        image={apiURL + '/' + image}
                        title={title}
                        className={classes.media}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {title}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                {user._id === params.id && (
                    <CardActions>
                        <IconButton onClick={deleteButton}>
                            <DeleteIcon />
                        </IconButton>
                    </CardActions>
                )}
            </Card>
            <Modal
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                className={classes.modal}
            >
                <Card>
                    <CardMedia
                        image={apiURL + '/' + image}
                        title={title}
                        className={classes.modalMedia}
                    />
                    <CardActions className={classes.closeButton}>
                        <IconButton onClick={() => setOpen(false)}>
                            <CancelPresentationIcon/>
                        </IconButton>
                    </CardActions>
                </Card>
            </Modal>
        </Grid>
    );
};

export default UserPhotoItem;